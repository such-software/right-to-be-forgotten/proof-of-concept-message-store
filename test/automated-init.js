// eslint-disable-next-line import/no-extraneous-dependencies
const test = require('blue-tape')
// eslint-disable-next-line import/no-extraneous-dependencies

const MessageStore = require('../lib')
const Session = require('./session')

const session = Session({ connectionString: 'postgres://postgres@localhost:5432/message_store' })
const messageStore = MessageStore({ session })

test.onFinish(() => {
  messageStore.stop()
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

module.exports = {
  messageStore
}
